import json
import pandas as pd
import os,sys
#Opening and loading the json 
f = open('result.json', encoding='utf8')
data = json.load(f)

#Retreive only the messages tag from json

msgs = data['messages']

# print(msgs)
dmain = pd.DataFrame(msgs)

#Create a new dataframe with only required columns (id,date,from,reply_to_message_id and text)
df_new = dmain.filter(items = ['id','date', 'from', 'reply_to_message_id' ,'text'])

# print(df_new)
#Retrieve all the answers by Admins
#Retrieve only if the length of text is greater than 8 and if it is a reply to another message, basically chk if reply_to_message_id > 0

df_ans = df_new.loc[df_new['from'].isin(['Sohini ITE', 'Durba ITE', 'Amina Ite','Srabanti','Roshan ITE']) & (df_new['text'].str.len() > 8) & (df_new["reply_to_message_id"] > 0), ['id','from','date','reply_to_message_id' ,'text']]

#List of words to be searched in the df_ans to eliminate those answers

dropList = ['pin','pinned','HAPPY LEARNING 🙂','how','when','which','what','good morning','good evening','goodafternoon','awesome','click','thanks','share','introduce','welcome','great','unfortunately','congratulations','congrats','wow']

#Eliminate all messages which has above words

df_ans = df_ans[~df_ans['text'].str.lower().str.contains('|'.join(dropList),na=False)]


#eliminate "?"

df_ans = df_ans[~df_ans['text'].str.contains("?", regex=False)]

#Retrieve all the questions
#List of words to be searched to retrieve qtns asked to Sohini

include_list =['tell','can','what','where','when','how','which','who','why','suggest','take','sir','madam']

df_ques = df_new.loc[~df_new['from'].isin(['Sohini ITE', 'Durba ITE', 'Amina Ite', 'Srabanti', 'Roshan ITE']) & (df_new['text'].str.lower().str.contains('|'.join(include_list),na=False)) , ['id','from','date', 'reply_to_message_id' ,'text']]
print(df_ques)
#List of words to be searched to remove unwanted qtns

remove_list =['pin','pinned','HAPPY LEARNING 🙂']


df_ques = df_ques[~df_ques['text'].str.lower().str.contains('|'.join(remove_list),na=False)]

#merge both the question and answer dataframe with reply_to_message_id from answer DF and Id col  

qtns = pd.merge(df_ques, df_ans, left_on='id', right_on='reply_to_message_id')

#Extract only the required columns from the merged o/p
qtns[['id_x','from_x','date_x','text_x','id_y','from_y','date_y','reply_to_message_id_y','text_y']]

#extracting only 2 cols Qs and As(text_x = question and text_y = answer) and loop thru and print out Q and A

qtns = qtns[['text_x','text_y','date_x','date_y','from_x','from_y']]

#Open the txt for writing

f = open(r'qna_03_05.txt','a', encoding='utf8'); 
sys.stdout=f

#using for loop

for index, row in qtns.iterrows():

    print("Question :" + str(row['from_x']) + str(row['date_x']) + str(row['text_x']))
    print("Answer :" + str(row['from_y']) + str(row['date_y']) + str(row['text_y'])+"\n")
  
